from splinter import Browser
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

browser = Browser('chrome')

url = 'https://devexpress.github.io/testcafe/example/'
browser.visit(url)

browser.find_by_id('developer-name').type('Yusli Farsani')

browser.is_element_present_by_id('populate')

browser.check('CI')

browser.check('background')

browser.check('tried-test-cafe')

browser.choose('os','MacOS')

browser.find_by_xpath('//*[@id="preferred-interface"]/option[3]').click()

browser.check('tried-test-cafe')

if browser.is_element_present_by_id('slider') is True:
    time.sleep(1)
    browser.find_by_id('slider').click()
    active_web_element = browser.driver.switch_to_active_element()
    active_web_element.send_keys(Keys.ARROW_LEFT)

browser.find_by_id('comments').type('Yusli was here')

browser.find_by_id('submit-button').click()

if browser.is_text_present('Thank you') is True:
    print('Bye')
    browser.quit()
